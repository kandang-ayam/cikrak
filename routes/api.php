<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Account\CityController;
use App\Http\Controllers\Account\FriendController;
use App\Http\Controllers\Account\PreferenceController;
use App\Http\Controllers\Account\ProfileController;
use App\Http\Controllers\Product\PackageController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::get('me', [ProfileController::class, 'me']);
    Route::put('me', [ProfileController::class, 'update']);

    Route::post('resend', [AuthController::class, 'resend']);
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'profile'], function ($router) {
	Route::group(['prefix' => 'photo'], function ($router) {
		Route::get('profile', [ProfileController::class, 'getProfilePhoto']);
        Route::post('upload', [ProfileController::class, 'uploadPhotos']);
	});

    Route::get('viewer', [ProfileController::class, 'myViewer']);
    Route::get('mytip', [ProfileController::class, 'myTip']);
    Route::get('favorite', [ProfileController::class, 'myFavorite']);
    Route::get('flirt', [ProfileController::class, 'myFlirt']);
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'friends'], function ($router) {
    Route::get('/', [FriendController::class, 'myFriends']);
    Route::post('/view', [FriendController::class, 'view']);
    Route::post('/tip', [FriendController::class, 'tip']);
    Route::post('/favorite', [FriendController::class, 'favorite']);
    Route::post('/flirt', [FriendController::class, 'flirt']);
});

Route::group(['middleware' => 'api', 'prefix' => 'region'], function ($router) {
    Route::get('city', [CityController::class, 'index']);
    Route::get('resend', function(){
        return view('auth.resend');
    });
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'preference'], function ($router) {
	Route::get('/', [PreferenceController::class, 'index']);
	Route::get('me', [PreferenceController::class, 'me']);
});

Route::group(['middleware' => ['auth:api', 'verified'], 'prefix' => 'package'], function ($router) {
    Route::get('/', [PackageController::class, 'index']);
});