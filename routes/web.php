<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin/home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('loginadmin', 'App\Http\Controllers\Admin\Auth\LoginController@index');
Route::get('photoktp', 'App\Http\Controllers\Admin\PhotoController@index');
Route::get('photoprofile', 'App\Http\Controllers\Admin\PhotoController@photoprofile')->name('photoprofile');
Route::get('feedback', 'App\Http\Controllers\Admin\FeedbackController@index');
Route::get('badwords', 'App\Http\Controllers\Admin\BadWordsController@index');
Route::get('appereance', 'App\Http\Controllers\Admin\AppereanceController@index');
Route::get('previllege', 'App\Http\Controllers\Admin\PrevillegeController@index');
Route::get('membership', 'App\Http\Controllers\Admin\ProductController@membership')->name('membership');
Route::get('gift', 'App\Http\Controllers\Admin\ProductController@gift')->name('gift');
Route::get('policy', 'App\Http\Controllers\Admin\PolicyNewsController@policy')->name('policy');
Route::get('news', 'App\Http\Controllers\Admin\PolicyNewsController@news')->name('news');
Route::get('listuser', 'App\Http\Controllers\Admin\ListController@listuser')->name('listuser');
Route::get('listadmin', 'App\Http\Controllers\Admin\ListController@listadmin')->name('listadmin');
Route::get('activityuser', 'App\Http\Controllers\Admin\ActivityController@activityuser')->name('activityuser');
Route::get('activityadmin', 'App\Http\Controllers\Admin\ActivityController@activityadmin')->name('activityadmin');