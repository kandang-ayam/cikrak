@extends('admin.adminlte'))
@section('content')
<div class="card">
    <div class="card-header" style="background-color: darkred; opacity: 0.8; color: white">
        <i class="fa fa-align-justify"></i>{{__('admin.news')}}
    </div>

    <div class="card-body">
        <table class="table table-responsive-sm table-bordered" id="data_users_reguler">
            <colgroup>
                <col span="1" style="width: 5%; height: 70px;">
                <col span="1" style="width: 15%; height: 70px;">
                <col span="1" style="width: 20%; height: 70px;">
                <col span="1" style="width: 35%; height: 70px;">
                <col span="1" style="width: 10%; height: 70px;">
            </colgroup>

            <thead>
                <tr>
                    <th>{{__('admin.no')}}</th>
                    <th>{{__('admin.title')}}</th>
                    <th>{{__('admin.news')}}</th>
                    <th>{{__('admin.image')}}</th>
                    <th>{{__('admin.action')}}</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('scripts')

@endpush