<footer class="main-footer">
    <strong>&copy; 2020 <a href="http://synchroza.com">Synchroza</a>.</strong>
    
    <div class="float-right d-none d-sm-inline-block">
      <p>{{__('resend.footer')}}</p> 
    </div>
  </footer>