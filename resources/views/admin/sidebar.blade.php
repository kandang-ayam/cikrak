<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div  class="brand-link">
      <img href="./" src="{{asset('img/brand/synchroza_logo.svg')}}" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 pt-3 mb-3 d-flex">
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="./" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.home')}}
              </p>
            </a>
          </li>
        </ul>
        </nav>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.user_management')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./photoktp" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.photo_ktp_user')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./photoprofile" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.photo_profile_user')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./feedback" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.feedback')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.content_management')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./appereance" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.appereance')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./badwords" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.bad_words')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.previllege_management')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./previllege" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.previllege')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.product_management')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./membership" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.membership')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./gift" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.gift')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.payment_management')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.payment_providers')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.payment_config')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.payment_group')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.transaction')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.gift_transaction')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.policy_news')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./policy" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.policy')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./news" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.news')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.list')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./listuser" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.user')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./listadmin" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.admin')}}</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link active" style="background-color: darkred !important;">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                {{__('admin.activity')}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./activityuser" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.user')}}</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./activityadmin" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{__('admin.admin')}}</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>