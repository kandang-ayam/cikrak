
<table style="border-radius: 20px; border: 2px solid #D11204;">
    <tr>
        <td align="center" style="padding: 20px 0 10px 0; border-bottom: 2px solid #D11204; border-top-right-radius:20px; border-top-left-radius:20px;">
            <img src="{{asset('img/brand/synchroza.png')}}" style="width:300px; height:auto;"/>
        </td>
    </tr>
    <tr>
        <td style="padding: 40px 30px 40px 30px;">
            <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; line-height:25px;">
                <tr>
                    <td>
                        <p style="margin: 0;">{{__('resend.hallo')}}</p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="margin: 0;">{{__('resend.click_button')}}</p>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 30px 0 30px 0; text-align: center !important;">
                        <button class="btn btn-warning" 
                        style="display: inline-block;
                            font-weight: 400;
                            text-align: center;
                            white-space: nowrap;
                            vertical-align: middle;
                            -webkit-user-select: none;
                            -moz-user-select: none;
                            -ms-user-select: none;
                            user-select: none;
                            border: 1px solid transparent;
                            padding: 0.375rem 0.75rem;
                            font-size: 0.875rem;
                            line-height: 1.5;
                            -- border-radius: 20px;
                            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out; 
                            border-radius: 20px; 
                            color: white; 
                            background-color:#D11204;">
                            {{__('resend.verify_email_address')}}</button>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="margin: 0;">{{__('resend.if_you_did_not_create_an_account_no_further_action_is_required')}}</p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="margin-top: 10px;">{{__('resend.regards')}}</p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="margin: 0;">{{__('resend.synchroza')}}</p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p style="margin-top: 10px; border-top: 2px solid #D11204;">{{__('resend.trouble_click')}} .................</p>         
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" width="100%" style="border-bottom-left-radius:20px; border-bottom-right-radius:20px; background-color:#D11204; line-height:25px; text-align: center;">
                <tr>
                    <td>
                    <p style="color:white; font-size:12px; margin:8px;">&copy 2020  {{__('resend.footer')}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>