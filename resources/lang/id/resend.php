<?php

return [
    'hallo' => 'Hallo!',
    'verify_email_address' => 'Verifikasi Alamat Email',
    'click_button' => 'Silakan klik tombol di bawah ini untuk memverifikasi alamat email Anda.',
    'if_you_did_not_create_an_account_no_further_action_is_required' => 'Jika Anda tidak membuat akun, tidak diperlukan tindakan lebih lanjut.',
    'regards' => 'Salam,',
    'synchroza' => 'Synchroza',
    'trouble_click' => 'Jika Anda mengalami masalah saat klik tombol "Verifikasi Alamat Email", salin dan tempel URL di bawah ini ke web browser Anda:',
    'footer' => 'Synchroza Situs Kencan Indonesia Hak cipta dilindungi Undang-Undang'
];
