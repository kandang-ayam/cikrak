<?php

return [
    'hallo' => 'Hallo!',
    'verify_email_address' => 'Verify Email Address',
    'click_button' => 'Please click the button below to verify your email address.',
    'if_you_did_not_create_an_account_no_further_action_is_required' => 'If you did not create an account, no further action is required.',
    'regards' => 'Regards,',
    'synchroza' => 'Synchroza',
    'trouble_click' => 'If you are having trouble clicking the "Verify Email Address" button, copy and paste the URL below into your web browser:',
    'footer' => 'Synchroza Indonesia Dating Site All Rights Reserved.'
];
