<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('product')->create('packages', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->integer('free_chat_per_day');
            $table->enum('view_profile', ['yes', 'no'])->default('no')->index();
            $table->enum('flirt', ['yes', 'no'])->default('no')->index();

            $table->enum('favorite', ['yes', 'no'])->default('no')->index();
            $table->integer('favorite_total')->default(0); // -1 for unlimited favorite
            
            $table->enum('gift', ['yes', 'no'])->default('no')->index();
            $table->integer('gift_total')->default(0); // -1 for unlimited gift

            $table->enum('add_friend', ['yes', 'no'])->default('no')->index();
            $table->integer('add_friend_total')->default(0); // -1 for unlimited gift

            $table->enum('upload_photo', ['yes', 'no'])->default('no')->index();
            $table->integer('upload_photo_total')->default(0); // -1 for unlimited gift

            $table->enum('message', ['yes', 'no'])->default('no')->index();
            $table->integer('message_total')->default(0); // -1 for unlimited gift

            $table->integer('heart_bonus')->default(0);
            $table->integer('price');
            $table->integer('heart_redeem');
            $table->string('image')->nullable();
            $table->integer('package_expired')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('product')->dropIfExists('packages');
    }
}
