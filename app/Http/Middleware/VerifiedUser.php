<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifiedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user()->profile;
        $mailVerifiedAt = $user->email_verified_at;
        $isVerified = $user->is_verified;

        if (!empty($mailVerifiedAt) && $isVerified) {
            return $next($request);
        }

        return $this->forbidden();
    }

    protected function forbidden()
    {
        $response = [
            'status' => false,
            'message' => 'Forbidden, unverified user',
            'data' => null
        ];

        return response($response, 403);
    }
}
