<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname'  => 'required|string',
            'email'     => 'required|string|email|max:100|unique:auth.users',
            'username'  => 'required|string',
            'password'  => 'required|string|confirmed|min:6',
            'gender'    => 'required',
            'dob'       => 'required|date|date_format:Y-m-d|before:-20 years|after:'. Carbon::create(1940)->isoFormat('YYYY-MM-DD'),
            'introduction'      => 'required|string',
            'self_description'  => 'required|string',
            'id_city'   => 'required|integer'
        ];
    }
}
