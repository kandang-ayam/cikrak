<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Account\City;

class CityController extends Controller
{
    public function index()
    {
    	return City::get();
    }
}
