<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\UpdateProfileRequest;
use App\Http\Requests\Account\UserPhotoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Account\UserPhoto;

class ProfileController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function me()
    {
        return auth()->user()->load('profile.city.province');
    }

    public function myViewer()
    {
        return auth()->user()->profile->viewers;
    }

    public function myTip()
    {
        return auth()->user()->profile->actorTip;
    }

    public function myFavorite()
    {
        return auth()->user()->profile->myFavorite;
    }

    public function update(UpdateProfileRequest $request)
    {
        auth()->user()->profile->update($request->all());

        return $request->all();
    }

    public function uploadPhotos(UserPhotoRequest $request)
    {
            $user= auth()->user()->profile->id;
            $imageAs=$request->input('image_as');
            //input anchor/ktp/profile
            if($imageAs == 'anchor'){
                $image_64 = $request->input('image'); //input variabel photo yang sudah encode base64
                $base_64 = explode('base64', $image_64)[1];
                $name = time().'.' . explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
                $path = 'public/' . $name;
                Storage::disk('local')->put($path, base64_decode($base_64), 'public');
                UserPhoto::insert([
                    'id_user'=> $user,
                    'name' => 'anchor'.'_'.$name,
                    'image_as'=> 'anchor']);
            }
            else if($imageAs == "ktp"){
                $image_64 = $request->input('image'); //input variabel photo yang sudah encode base64
                $base_64 = explode('base64', $image_64)[1];
                $name = time().'.' . explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
                $path = 'public/' . $name;
                Storage::disk('local')->put($path, base64_decode($base_64), 'public');
                UserPhoto::insert([
                    'id_user'=> $user,
                    'name' => 'ktp'.'_'.$name,
                    'image_as'=> 'ktp']);
            }
            else if($imageAs == "profile"){
                $image_64 = $request->input('image'); //input variabel photo yang sudah encode base64
                $base_64 = explode('base64', $image_64)[1];
                $name = time().'.' . explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];
                $path = 'public/' . $name;
                Storage::disk('local')->put($path, base64_decode($base_64), 'public');
                UserPhoto::insert([
                    'id_user'=> $user,
                    'name' => 'profile'.'_'.$name,
                    'image_as'=> 'profile']);
            }

    }

    public function myFlirt()
    {
        return auth()->user()->profile->flirters;
    }

    public function getProfilePhoto()
    {
        return auth()->user()->profile;
    }
}
