<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Account\Friend;
use Illuminate\Http\Request;
use App\Models\User;

class FriendController extends Controller
{
    private $friend;

    function __construct(Friend $friend)
    {
        $this->friend = $friend;
    }

    public function myFriends()
    {
        return $this->friend->myFriends();
    }

    public function view(Request $request)
    {
        $idUser = $request->input('id_user');

        auth()->user()->profile->views()->sync($idUser);

        return 'success';
    }
    public function tip(Request $request)
    {
        $idUser = $request->input('id_user');

        auth()->user()->profile->tip()->sync($idUser);

        return 'success';
    }

    public function favorite(Request $request)
    {
        $idUser = $request->input('id_user');

        auth()->user()->profile->favorite()->sync($idUser);

        return 'success';
    }

    public function flirt(Request $request)
    {
        $idUser = $request->input('id_user');

        auth()->user()->profile->flirt()->sync($idUser);

        return 'success';
    }
}
