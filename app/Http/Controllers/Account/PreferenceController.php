<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Models\Account\Preference;
use App\Models\Account\PreferenceDetail;
use Illuminate\Http\Request;

class PreferenceController extends Controller
{
	public function index()
	{
		return Preference::with('details')->get();
	}

	public function me()
	{
		$preferences = Preference::with('details')->get();

		$preferences = $preferences->map(function($item) {
			$item->details->makeHidden('my_preference');

			return $item;
		});

		return $preferences;
	}
}
