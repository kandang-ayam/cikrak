<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Http\Requests\Auth\ResendVerificationRequest;
use App\Models\Account\User;
use App\Models\Auth\User as UserAuth;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResendVerification;
use \DB;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function login(LoginRequest $request)
    {
        if (! $token = auth()->attempt($request->validated()))
        {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    public function register(RegistrationRequest $request)
    {
        $userData = $request->except('password');

        $password = $request->get('password');

        DB::transaction(function() use ($userData, $password)
        {
            $user = User::create($userData);

            $user->auth_user()->create(['password' => bcrypt($password)]);
        });

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $userData
        ], 201);
    }

    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    public function resend(ResendVerificationRequest $request)
    {
        $email = $request->get('email');

        $user = UserAuth::where('email', $email)->first();

        if (!empty($user)) {
            Mail::to($email)->send(new ResendVerification($user->profile));
        }
    }
}
