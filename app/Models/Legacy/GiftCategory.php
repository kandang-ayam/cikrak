<?php

namespace App\Models\Legacy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GiftCategory extends Model
{
    use HasFactory;

    protected $connection = 'legacy';

    protected $table = 'gift_categories';

    public function products()
    {
    	return $this->hasMany(Product::class, 'gift_category', 'id');
    }
}
