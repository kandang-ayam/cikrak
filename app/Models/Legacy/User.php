<?php

namespace App\Models\Legacy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $connection = 'legacy';

    protected $table = 'users';

    public function detail()
    {
    	return $this->hasOne(UserDetail::class, 'iduser', 'id');
    }
}
