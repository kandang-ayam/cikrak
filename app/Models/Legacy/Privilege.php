<?php

namespace App\Models\Legacy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    use HasFactory;

    protected $connection = 'legacy';

    protected $table = 'tprevilege';

    public function detail()
    {
    	return $this->hasOne(Product::class, 'id_prev', 'id_prev')->where('kat_produk', 1);
    }
}
