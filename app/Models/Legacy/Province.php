<?php

namespace App\Models\Legacy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use HasFactory;

    protected $connection = 'legacy';

    protected $table = 'tprovinsi';


    public function cities()
    {
    	return $this->hasMany(City::class, 'idprov', 'id_prov');
    }
}
