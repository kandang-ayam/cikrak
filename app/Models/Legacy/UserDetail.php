<?php

namespace App\Models\Legacy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    use HasFactory;

    protected $connection = 'legacy';

    protected $table = 'tdetailuser';

    public function user()
    {
    	return $this->belongsTo(User::class, 'iduser', 'id');
    }
}
