<?php

namespace App\Models\Legacy;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlockedWord extends Model
{
    use HasFactory;

    protected $connection = 'legacy';

    protected $table = 'blocked_words';
}
