<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    use HasFactory;

    protected $connection = 'account';

    protected $table = 'friends';

    protected $fillable = ['id_user', 'id_friend', 'status', 'is_favorite'];

    protected $hidden = ['created_at', 'updated_at'];

    public function myFriends()
    {
        $userId = auth()->user()->profile->id;

        $datas = $this->where('status', 'confirmed')
                    ->where(function($q) use ($userId) {
                        $q->where('id_user', $userId)
                            ->orWhere('id_friend', $userId);
                    })
                    ->get();

        $followerIds = $datas->where('id_user', '<>', $userId)->pluck('id_user');

        $followeeIds = $datas->where('id_friend', '<>', $userId)->pluck('id_friend');

        $friendIds = $followerIds->merge($followeeIds);

        $friends = User::select('id', 'username')->whereIn('id', $friendIds)->get();

        return $friends;
    }
}
