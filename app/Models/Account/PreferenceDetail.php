<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PreferenceDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'account';

    protected $table = 'preference_details';

    protected $fillable = ['id_preference', 'name'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $appends = ['is_selected'];

    public function preference()
    {
    	return $this->belongsTo(Preference::class, 'id_preference', 'id');
    }

    public function user_preferences()
    {
        return $this->belongsToMany(User::class, 'user_preferences', 'id_preference_detail', 'id_user');
    }

    public function my_preference()
    {
    	return $this->user_preferences()->wherePivot('id_user', auth()->user()->profile->id);
    }

    public function getIsSelectedAttribute()
    {
    	return $this->my_preference->isNotEmpty();
    }
}
