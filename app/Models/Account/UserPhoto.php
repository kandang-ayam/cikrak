<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPhoto extends Model
{
    use HasFactory;

    protected $connection = 'account';

    protected $table = 'user_photos';

    protected $fillable = ['id_user', 'name', 'image_as', 'is_approved'];

    protected $hidden = ['created_at', 'updated_at'];

    protected $casts = [
    	'is_approved' => 'boolean'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class, 'id_user', 'id');
    }

}
