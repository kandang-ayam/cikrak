<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class District extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'account';

    protected $table = 'districts';

    protected $fillable = ['id_city', 'name'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function city()
    {
    	return $this->belongsTo(City::class, 'id_city', 'id');
    }
}
