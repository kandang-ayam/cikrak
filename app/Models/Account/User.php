<?php

namespace App\Models\Account;

use App\Models\Auth\User as UserAuth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'account';

    protected $table = 'users';

    protected $fillable = [
    	'fullname',
    	'email',
    	'email_verified_at',
        'is_verified',
    	'username',
    	'password',
    	'gender',
    	'dob',
    	'introduction',
    	'self_description',
    	'id_city'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $appends = [
        'photo_profile',
        'gallery_photos',
        'myflirt',
        'flirt',
        'total_viewers',
        'total_views'
    ];

    protected $casts = [
        'is_verified' => 'boolean',
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'id_city', 'id');
    }

    public function auth_user()
    {
        return $this->hasOne(UserAuth::class, 'email', 'email');
    }

    public function photos()
    {
        return $this->hasMany(UserPhoto::class, 'id_user', 'id');
    }

    public function views()
    {
        return $this->belongsToMany(User::class, 'profile_views', 'id_viewer', 'id_user')
            ->withTimestamps();
    }

    public function viewers()
    {
        return $this->belongsToMany(User::class, 'profile_views', 'id_user', 'id_viewer');
    }

    public function flirt()
    {
        return $this->belongsToMany(User::class, 'flirts', 'id_flirter', 'id_user');
    }

    public function flirters()
    {
        return $this->belongsToMany(User::class, 'flirts', 'id_user',  'id_flirter');
    }

    public function getMyFlirtAttribute()
    {
        return  $this->flirt()->count();
    }

    public function getFlirtAttribute()
    {
        return $this->flirters()->count();
    }

    public function tip()
    {
        return $this->belongsToMany(User::class, 'tip', 'id_user', 'id_actor');
    }

    public function actorTip()
    {
        return $this->belongsToMany(User::class, 'tip', 'id_actor', 'id_user');
    }

    public function favorite()
    {
        return $this->belongsToMany(User::class, 'favorites', 'id_actor', 'id_user')
            ->withTimestamps();
    }

    public function myFavorite()
    {
        return $this->belongsToMany(User::class, 'favorites', 'id_user', 'id_viewer');
    }

    public function getPhotoProfileAttribute()
    {
        $photo = $this->photos()
                    ->where('image_as', 'profile')
                    ->where('is_approved', 1)
                    ->first();

        return empty($photo) ? null : $photo->makeHidden(['id_user', 'is_approved']);
    }

    public function getTotalViewersAttribute()
    {
        return $this->viewers()->count();

    }

    public function getTotalViewsAttribute()
    {
        return $this->views()->count();

    }

    public function getGalleryPhotosAttribute()
    {
        return $this->photos()
                ->where('image_as', 'galery')
                ->where('is_approved', 1)
                ->get()
                ->makeHidden(['id_user', 'is_approved']);
    }
}
