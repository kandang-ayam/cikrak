<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preference extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'account';

    protected $table = 'preferences';

    protected $fillable = ['name', 'is_multiple_select'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $casts = [
        'is_multiple_select' => 'boolean',
    ];

    public function details()
    {
    	return $this->hasMany(PreferenceDetail::class, 'id_preference', 'id');
    }
}
