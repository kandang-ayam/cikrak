<?php

namespace App\Models\Account;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPreference extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'account';

    protected $table = 'user_preferences';

    protected $fillable = ['id_user', 'id_preference_detail'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
