<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Badword extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'common';

    protected $table = 'badwords';

    protected $fillable = ['word', 'description'];
}
