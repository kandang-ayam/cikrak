<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GiftCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'product';

    protected $table = 'gift_categories';

    public function gifts()
    {
    	return $this->hasMany(Gift::class, 'id_gift_category', 'id');
    }
}
