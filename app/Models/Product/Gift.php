<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gift extends Model
{
    use HasFactory, SoftDeletes;

    protected $connection = 'product';

    protected $table = 'gifts';

    public function category()
    {
    	return $this->belongsTo(GiftCategory::class, 'id', 'id_gift_category');
    }
}
