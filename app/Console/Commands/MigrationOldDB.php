<?php

namespace App\Console\Commands;

use DB;
use App\Models\Account\City as NewCity;
use App\Models\Account\Province as NewProvince;
use App\Models\Account\Membership;
use App\Models\Account\User as NewUser;
use App\Models\Auth\User as NewAuthUser;
use App\Models\Common\Badword;
use App\Models\Common\News as NewNews;
use App\Models\Common\Policy as NewPolicy;
use App\Models\Product\Gift as NewGift;
use App\Models\Product\GiftCategory as NewCategory;
use App\Models\Product\Package;
use App\Models\Legacy\BlockedWord;
use App\Models\Legacy\City;
use App\Models\Legacy\Favorite;
use App\Models\Legacy\GiftCategory;
use App\Models\Legacy\News;
use App\Models\Legacy\Policy;
use App\Models\Legacy\Privilege;
use App\Models\Legacy\Province;
use App\Models\Legacy\ProfileView;
use App\Models\Legacy\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class MigrationOldDB extends Command
{

    protected $signature = 'migration:synchroza';

    protected $description = 'Migration old database to microservice pattern';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->migrateRegion();
        $this->migrateUser();
        $this->migrateViewProfile();
        $this->migrateFavorite();
        $this->migrateGifts();
        $this->migratePackages();
        $this->migrateMembership();
        $this->migrateBadwords();
        $this->migratePolicy();
        $this->migrateNews();
    }

    protected function migrateRegion()
    {
        $provinces = Province::get();

        $newProvinces = [];

        $newCities = [];

        $now = date('Y-m-d H:i:s');

        foreach ($provinces as $province) {
            $newProvinces[] = [
                'id' => $province->id_prov,
                'name' => $province->nm_prov,
                'created_at' => $now,
                'updated_at' => $now
            ];

            $cities = $province->cities;

            foreach ($cities as $city) {
                $newCities[] = [
                    'id' => $city->id_kabkot,
                    'id_province' => $city->idprov,
                    'name' => $city->nm_kabkot,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }

        NewCity::truncate();
        NewProvince::truncate();

        NewCity::insert($newCities);
        NewProvince::insert($newProvinces);
    }

    protected function migrateUser()
    {
        $users = User::get();

        $newUsers = [];

        $newAuthUsers = [];

        foreach ($users as $user) {
            $newUsers[] = [
                'fullname' => $user->detail->name,
                'email' => $user->email,
                'email_verified_at' => $user->email_verified_at,
                'is_verified' => $user->detail->stats_approved,
                'username' => $user->detail->nickname,
                'gender' => $user->detail->gender == 1 ? 'Male' : 'Female',
                'dob' => $user->detail->bod,
                'introduction' => $user->detail->introduction,
                'self_description' => $user->detail->bio,
                'id_city' => $user->detail->id_kabkot,
                'created_at' => $user->created_at,
                'updated_at' => $user->updated_at
            ];

            $newAuthUsers[] = [
                'email' => $user->email,
                'password' => $user->password
            ];

        }

        NewUser::truncate();
        NewAuthUser::truncate();

        NewUser::insert($newUsers);
        NewAuthUser::insert($newAuthUsers);
    }

    protected function migrateViewProfile()
    {
        $viewProfiles = ProfileView::get();

        $newViewProfiles = [];

        $now = date('Y-m-d H:i:s');

        foreach ($viewProfiles as $view) {
            $newViewProfiles[] = [
                'id_viewer' => $view->id_user,
                'id_user' => $view->id_member,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        DB::connection('account')->table('profile_views')->truncate();
        DB::connection('account')->table('profile_views')->insert($newViewProfiles);
    }

    protected function migrateFavorite()
    {
        $favorites = Favorite::get();

        $newFavorites = [];

        $now = date('Y-m-d H:i:s');

        foreach ($favorites as $favorite) {
            $newFavorites[] = [
                'id_actor' => $favorite->id_user,
                'id_user' => $favorite->id_member,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        DB::connection('account')->table('favorites')->truncate();
        DB::connection('account')->table('favorites')->insert($newFavorites);
    }

    protected function migrateGifts()
    {
        $categories = GiftCategory::get();

        $newCategories = [];

        $newGifts = [];

        $now = date('Y-m-d H:i:s');

        foreach ($categories as $category) {
            $newCategories[] = [
                'id' => $category->id,
                'name' => $category->name,
                'image' => $category->photo,
                'created_at' => $now,
                'updated_at' => $now
            ];

            foreach ($category->products as $product) {
                $newGifts[] = [
                    'id_gift_category' => $category->id,
                    'name' => $product->package,
                    'image' => $product->package_photo,
                    'heart_price' => $product->price_diamond,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
        }

        NewGift::truncate();
        NewGift::insert($newGifts);

        NewCategory::truncate();
        NewCategory::insert($newCategories);
    }

    protected function migratePackages()
    {
        $privileges = Privilege::get();

        $now = date('Y-m-d H:i:s');

        $packages = [];

        foreach ($privileges as $privilege) {
            $packages[] = [
                'id' => $privilege->id_prev,
                'id_membership' => $privilege->kategori,
                'name' => $privilege->nama,
                'description' => $privilege->detail->package,
                'free_chat_per_day' => $privilege->free_chat,
                'view_profile' => $privilege->view_profile,
                'gift' => $privilege->gift,
                'gift_total' => $privilege->unlimited_gift ? -1 : (int) $privilege->jml_gift,
                'add_friend' => $privilege->friend,
                'add_friend_total' => $privilege->unlimited_friend ? -1 : (int) $privilege->jml_friend,
                'upload_photo' => $privilege->photo,
                'upload_photo_total' => $privilege->unlimited_photo ? -1 : (int) $privilege->jml_photo,
                'message' => $privilege->message,
                'message_total' => $privilege->unlimited_message ? -1 : (int) $privilege->jml_message,
                'heart_bonus' => (int) $privilege->bonus_heart,
                'price' => $privilege->detail->price_nom,
                'heart_redeem' => 15,
                'image' => $privilege->detail->package_photo,
                'package_expired' => (int) $privilege->detail->package_exp,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        Package::truncate();
        Package::insert($packages);
    }

    protected function migrateMembership()
    {
        $now = date('Y-m-d H:i:s');

        $memberships = [
            [
                'id' => 1,
                'name' => 'Basic',
                'level' => 1,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 2,
                'name' => 'Premium',
                'level' => 2,
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'id' => 3,
                'name' => 'VIP',
                'level' => 3,
                'created_at' => $now,
                'updated_at' => $now
            ]
        ];

        Membership::truncate();
        Membership::insert($memberships);
    }

    protected function migrateBadwords()
    {
        $now = date('Y-m-d H:i:s');

        $words = BlockedWord::get();

        $badWords = [];

        foreach ($words as $word) {
            $badWords[] = [
                'word' => $word->word,
                'description' => $word->description,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        Badword::truncate();
        Badword::insert($badWords);
    }

    protected function migratePolicy()
    {
        $policies = Policy::get();

        $newPolicies = [];

        foreach ($policies as $policy) {
            $newPolicies[] = [
                'content' => $policy->content,
                'created_at' => $policy->insert_timestamps,
                'updated_at' => $policy->insert_timestamps
            ];
        }

        NewPolicy::truncate();
        NewPolicy::insert($newPolicies);
    }

    protected function migrateNews()
    {
        $news = News::get();

        $newNews = [];

        foreach ($news as $new) {
            $newNews[] = [
                'title' => $new->title,
                'content' => $new->content,
                'image' => $new->image,
                'created_at' => $new->insert_timestamps,
                'updated_at' => $new->insert_timestamps
            ];
        }

        NewNews::truncate();
        NewNews::insert($newNews);
    }
}
